Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: "sessions#home"
  get '/login', controller: :sessions, action: :new
  post '/login', controller: :sessions, action: :create
  get '/signup', controller: :users, action: :new
  post '/signup', controller: :users, action: :create
  get '/logout', controller: :sessions, action: :destroy
  get '/clock-in', controller: :clocking_events, action: :clock_in
  post '/clock-out', controller: :clocking_events, action: :clock_out
  post '/clocking-events/:id', controller: :clocking_events, action: :update, as: :clocking_event
  resources :users, only: [:index, :show]
  resources :clocking_events, only: [:edit]
end
