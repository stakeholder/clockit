# ClockIT
ClockIT is a simple time tracker and timesheet app.

### Link to hosted application

https://clockitz.herokuapp.com/

## How I approached the code challenge
I approached the development of the application in the following stages

### Planning and Analysis
1. Highlighted and analysed requirements specified in the challenge.

2. Broke requirements into functionalities in order to understand what the application is meant to achieve.

3. Broke functionalities into tasks.

### Design

  1. ##### Database:
    * Decided on database to use for the application.

    * Identified the actors within the application.

    * Identified necessary attributes (fields) for each actor.

    * Established the relationships between actors.

    * Designed the schema for the database.


  2. ##### Application:
    * Identified the routes the application needed to have.

    * Decided on the authentication system to adopt for the application.

    * Identified the necessary controllers needed to be created.

    * Identified the models needed based off the database design.

    * Identified the validations need to be put in place.

    * Decided on the css library to use.

    * Identified the UI pages needed to be implemented

  3.  ##### Implementation:
    *  Created migrations for models

    * Implemented the Authentication System which involved creating the registration, login and logout features.

    * Implemented the clock in and clock out features.

    * Implemented the listing and updating of time entries.

NB: I was able to test the application manually in order to identify bugs and places of improvement, but I was unable to write unit test due to time constraint.

## Did your plans change as you began coding?
Yes my plans changed when I began coding. The initial resolution was to implement all the business logics before designing the UI pages. This brought challenges in terms of manually testing features from a user perspective during implementation.

To fix this, I categorised the implementation based on features as stated in the implementation phase above. Then I implemented both the UI pages and business logic per feature.


## Describe the schema design you chose. Why did you choose this design? What other alternatives did you consider?
I chose the logical schema design. It is a data model for a specific problem domain expressed in terms of data structures such as relational tables and columns, object-oriented classes, etc. I chose it because it can be designed independently of a particular database or storage technology.

### If you were given another day to work on this, how would you spend it? What if you were given a month?

If I was given another day, I will spend it majorly on writing test cases for the application.

#####  If I was given a month:
* I will adopt the micro-service architecture for the application, creating a clocking service that can be consumed with different platforms.
* The Single Page Application approach will be adopted for the UI leveraging on the React library.
* The application will be expanded to cater for multiple daycare centres and multiple types of users with different roles.
* Optimisation of the application.
* Implement an admin dashboard for admin users to manage all clocking events.
* Implementation of password reset feature and notification feature via email.
