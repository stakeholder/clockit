class ClockingEventsController < ApplicationController
  before_action :logged_in_user

  def clock_in
    ClockingEvent.create(clock_in_time: DateTime.now, user_id: current_user.id)
    flash[:notice] = "Clocked In"
    redirect_to user_url current_user
  end

  def clock_out
    clocking_event = current_user.clocking_events.last
    clocking_event.clock_out_time = DateTime.now
    clocking_event.clock_out_reason = clock_out_params[:clock_out_reason]
    clocking_event.save
    flash[:notice] = "Clocked Out"
    redirect_to user_url current_user
  end

  def edit
    @clocking_event = ClockingEvent.find_by(id: params[:id], user_id: current_user.id)
    @user = current_user
    render template: "users/show"
  end

  def update
    @clocking_event = ClockingEvent.find_by(id: params[:id], user_id: current_user.id)
    clock_in_time = get_datetime(params[:clock_in_date], params[:clock_in_time])
    clock_out_time = get_datetime(params[:clock_out_date], params[:clock_out_time])

    if clock_in_time > DateTime.now
      flash[:error] = "Clock in datetime is beyond current datetime"
      redirect_back fallback_location: { action: :edit, id: params[:id]} and return
    end

    if clock_out_time && (clock_out_time > clock_in_time)
      flash[:error] = "Clock in datetime is beyond clock out datetime"
      redirect_back fallback_location: { action: :edit, id: params[:id]} and return
    end

    result = @clocking_event.update(
      clock_in_time: clock_in_time,
      clock_out_time: clock_out_time,
      clock_out_reason: params[:clock_out_reason]
    )

    unless result
      flash[:error] = "Failed to update"
      redirect_back fallback_location: { action: :edit, id: params[:id]} and return
    end
    flash[:success] = "Time entry was updated successfully"
    redirect_to user_url current_user
  end

  private

  def clock_out_params
    params.permit(:clock_out_reason)
  end

  def update_params
    params.permit(
      :clock_in_date, :clock_out_date, :clock_in_time,
      :clock_out_time, :clock_out_reason
    )
  end

  def get_datetime(date, time)
    DateTime.strptime("#{date} #{time}", '%Y-%m-%d %H:%M') unless date.nil? && time.nil?
  end

end
