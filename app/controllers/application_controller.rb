class ApplicationController < ActionController::Base
  include SessionHelper

  private

    def logged_in_user
      unless logged_in?
        flash[:error] = "Please log in"
        redirect_to login_url
      end
    end
end
