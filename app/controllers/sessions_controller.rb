class SessionsController < ApplicationController
  def new
    redirect_to user_url current_user and return if logged_in?
    render template: "sessions/new"
  end;

  def home
    render template: "home"
  end

  def create
    user_facade = Controllers::UsersFacade.new
    @user = user_facade.authenticate_user(login_params[:email].downcase, login_params[:password])
    respond_to do |format|
      format.html do
        unless user_facade.errors.empty?
          flash[:errors] = user_facade.errors
          redirect_to login_url, status: 401
          return
        end
        log_in @user
        flash[:success] = "Welcome to ClockIT"
        redirect_to user_url @user, status: 200
      end
    end
  end

  def destroy
    logout
    redirect_to root_url
  end

  private

    def login_params
      params.permit(:email, :password)
    end
end
