class UsersController < ApplicationController
  before_action :logged_in_user, only: [:show]

  def new
    @user = User.new
  end

  def create
    user_facade = Controllers::UsersFacade.new
    user_facade.create_user user_params
    respond_to do |format|
      format.html do
        unless user_facade.errors.empty?
          flash[:errors] = user_facade.errors
          render :new, status: 400
          return
        end
        flash[:success] = "Registration was successful. Please log in with your credentials"
        render template: "sessions/new" , status: 200
      end
    end
  end

  def show
    @user = current_user
    render "users/show"
  end

  private

  def user_params
    params.permit(
      :email, :password, :password_confirmation,
      :first_name, :last_name, :middle_name
    )
  end
end
