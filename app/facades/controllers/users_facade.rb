module Controllers
  class UsersFacade
    attr_accessor :errors, :user

    def initialize
      @errors = []
      @user = nil
    end


    def create_user(params)
      validate_email_availability params[:email]
      validate_password_matches params[:password], params[:password_confirmation]
      return unless errors.empty?
      @user = User.new params
      unless @user.save
        errors << "Registration Failed! An unexpected error occurred"
        return
      end
      @user.reload
    end

    def authenticate_user(email, password)
      user = User.find_by_email(email)
      return user if user && user.authenticate(password)
      errors << "Invalid email/password combination"
    end

    private

    def validate_email_availability(email)
      errors << "This email has already been registered" if User.find_by_email email.downcase
    end

    def validate_password_matches(password, password_confirmation)
      errors << "Password and confirm password do not match" if (password != password_confirmation)
    end

  end
end
