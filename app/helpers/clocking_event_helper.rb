module ClockingEventHelper
  def format_time(clocking_event_time)
    clocking_event_time.localtime.to_formatted_s(:long) unless clocking_event_time.nil?
  end

  def already_clocked_in?(user)
    return false if user.clocking_events.empty?
    user.clocking_events.last.clock_out_time.nil?
  end

  def extract_date(datetime)
    return if datetime.nil?
    datetime.localtime.strftime("%Y-%m-%d")
  end

  def extract_time(datetime)
    return if datetime.nil?
    datetime.localtime.strftime("%H:%M")
  end
end
