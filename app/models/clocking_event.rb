class ClockingEvent < ApplicationRecord
  belongs_to :user
  validates_presence_of :clock_in_time
end
