class User < ApplicationRecord
  has_secure_password
  has_many :clocking_events
  validates_presence_of :email, format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i }
  validates_uniqueness_of :email
  validates_presence_of :password, length: { minimum: 6 }
  validates_uniqueness_of :last_name, :first_name
  before_save { self.email = email.downcase }
end
