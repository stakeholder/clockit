# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_04_22_194918) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clocking_events", force: :cascade do |t|
    t.datetime "clock_in_time", null: false
    t.datetime "clock_out_time"
    t.text "clock_out_reason"
    t.integer "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", limit: 30, null: false
    t.string "password_digest", null: false
    t.string "first_name", limit: 15, null: false
    t.string "last_name", limit: 15, null: false
    t.string "middle_name", limit: 15, default: ""
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email"
  end

  add_foreign_key "clocking_events", "users"
end
