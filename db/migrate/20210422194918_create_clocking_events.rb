class CreateClockingEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :clocking_events do |t|
      t.datetime :clock_in_time, null: false
      t.datetime :clock_out_time
      t.text :clock_out_reason
      t.integer :user_id, null: false
      t.timestamps
    end
    add_foreign_key :clocking_events, :users
  end
end
