class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :email, null: false, limit: 30
      t.string :password_digest, null: false
      t.string :first_name, null: false, limit: 15
      t.string :last_name, null: false, limit: 15
      t.string :middle_name, default: "", limit: 15
      t.timestamps
    end
    add_index :users, :email
  end
end
